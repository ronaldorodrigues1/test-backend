<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromArray;

class ProductsExport implements FromArray, WithHeadings
{
    protected $products;


    public function headings(): array
    {
        return [
            'Mês',
            'Categoria',
            'Nome',
            'Quantidade'
        ];
    }

    public function __construct(array $products)
    {
        $this->products = $products;
    }

    public function array(): array
    {
        return $this->products;
    }
}
