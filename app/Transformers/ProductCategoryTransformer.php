<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\ProductCategory;

/**
 * Class ProductCategoryTransformer.
 *
 * @package namespace App\Transformers;
 */
class ProductCategoryTransformer extends TransformerAbstract
{
    /**
     * Transform the ProductCategory entity.
     *
     * @param \App\Entities\ProductCategory $model
     *
     * @return array
     */
    public function transform(ProductCategory $model)
    {
        return [
            'id' => (int)$model->id,
            'name' => $model->name,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
