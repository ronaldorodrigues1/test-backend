<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Product;
use App\Helpers\DateHelper;
/**
 * Class ProductTransformer.
 *
 * @package namespace App\Transformers;
 */
class ProductTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['category'];
    /**
     * Transform the Product entity.
     *
     * @param \App\Entities\Product $model
     *
     * @return array
     */
    public function transform(Product $model)
    {
        $dateHelper = new DateHelper;
        
        return [
            'id' => (int)$model->id,
            'name' => $model->name,
            'month' => $dateHelper->getMonthNameById($model->month),
            'quantity' => (int)$model->quantity,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }

    public function includeCategory(Product $model)
    {
        return $this->item($model->category, new ProductCategoryTransformer());
    }

}
