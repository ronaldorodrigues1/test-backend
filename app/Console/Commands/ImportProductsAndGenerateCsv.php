<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\ProductLoadService;

class ImportProductsAndGenerateCsv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:importAndSaveOrExport {file : File php with return array} {--exportOrInsertDatabase= : Insert On Database (1) or not (0)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa os produtos do arquivo baseado em um arquivo.php e exporta ou insere no banco de dados';

    private $service;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ProductLoadService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = $this->validateInputOptions();

        if ($this->service->load($data['file'], $data['flag'])) {

            if ($data['flag']) {
                return $this->info('Success, the file is was imported.');
            }

            return $this->info('Success, the file is in the folder compras do ano.');
        }

        return $this->error('Sorry! Was not possible to import this list of products.');
    }

    private function validateInputOptions()
    {
        $file = $this->argument('file');
        $flag = $this->option('exportOrInsertDatabase');

        if (is_null($file) && !$file) {
            return $this->error('Insert the path of the file *.php using products:importAndExport {file} {--exportUsingDatabase=0 | 1} ');
        }

        if (is_null($flag)) {
            return $this->error('Insert the type of importation using --exportUsingDatabase= : Use database to load the csv (1) or not (0).');
        }

        return ['file' => $file, 'flag' => $flag];
    }
}
