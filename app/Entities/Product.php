<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Presentable;
use Prettus\Repository\Traits\PresentableTrait;
/**
 * Class Product.
 *
 * @package namespace App\Entities;
 */
class Product extends Model implements Presentable {

    use PresentableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'month', 'quantity', 'category_id'
    ];

    public function category()
    {
        return $this->belongsTo(\App\Entities\ProductCategory::class);
    }
 

}
