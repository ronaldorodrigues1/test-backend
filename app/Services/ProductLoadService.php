<?php

namespace App\Services;

use Excel;
use App\Helpers\DateHelper;
use App\Helpers\SpellerHelper;
use App\Exports\ProductsExport;
use App\Repositories\ProductRepository;
use App\Repositories\ProductCategoryRepository;

class ProductLoadService
{

    protected $speller;
    protected $months;
    protected $productRepository;
    protected $categoryRepository;


    public function __construct(DateHelper $dateHelper, SpellerHelper $speller, ProductRepository $productRepository, ProductCategoryRepository $categoryRepository)
    {
        $this->speller = $speller;
        $this->productRepository = $productRepository;
        $this->dateHelper = $dateHelper;
        $this->months   = $this->dateHelper->getMonths();
        $this->categoryRepository   = $categoryRepository;
    }

    public function load($file, $flag)
    {
        $products =  $this->getProductsFromFile($file);

        $listOfShopping = $this->validateArrayOfProductsAndFormatToExport($products);

        if (!$flag) {
            return $this->exportProductsWithoutDatabase($listOfShopping);
        }

        return $this->importProductsToDatabase($listOfShopping);
    }


    private function validateArrayOfProductsAndFormatToExport($listOfShopping)
    {
        if (!is_array($listOfShopping) || is_null($listOfShopping)) {
            return false;
        }

        $cleanedListOfShopping = $this->cleanArrayToHaveBetterOrderByMonthAndCorrectWords($listOfShopping);
        $productPerMonthOrdered = $this->orderByMonth($cleanedListOfShopping);
        $productOrderByCategoryAfterMonth = $this->orderByCategory($productPerMonthOrdered);
        $formatedProductsByNameAndQuantity = $this->formatProductsByNameAndQuantity($productOrderByCategoryAfterMonth);
        return $this->formatRowType($formatedProductsByNameAndQuantity);
    }


    private function importProductsToDatabase($listOfShipping)
    {
        return $this->loadProductsOnDatabase($listOfShipping);
    }

    public function loadProductsOnDatabase($listOfShopping)
    {

        foreach ($listOfShopping as $product) {
            $categoryId = $this->getCategoryIdByName($product['category']);

            $this->productRepository->create([
                'name' => $product['name'],
                'quantity' => $product['quantity'],
                'month' => $this->dateHelper->getMonthIdByName($product['month']),
                'category_id' => $categoryId,
            ]);
        }

        return true;
    }

    private function getCategoryIdByName($categoryName)
    {
        $category = $this->categoryRepository->firstOrCreate(['name' => $categoryName]);
        return $category->id;
    }

    private function formatRowType($listOfShipping)
    {
        $format = [];
        $count = 0;

        foreach ($listOfShipping as $month => $categories) {
            foreach ($categories as $category => $products) {
                foreach ($products as $product) {
                    $format[$count]['month'] = $month;
                    $format[$count]['category'] = $category;
                    $format[$count]['name'] = $product['name'];
                    $format[$count]['quantity'] = $product['quantity'];
                    $count++;
                }
            }
        }
        return $format;
    }

    private function exportProductsWithoutDatabase($listOfShipping)
    {
        return Excel::store(new ProductsExport($listOfShipping), env('PRODUCT_FILE_NAME', 'compras-do-ano.xlsx'));
    }

    private function getProductsFromFile($file)
    {
        if (file_exists($file)) {
            return include($file);
        }

        return false;
    }

    private function cleanArrayToHaveBetterOrderByMonthAndCorrectWords($listOfShopping)
    {
        $productsPerMonth = [];

        foreach ($listOfShopping as $month => $categories) {
            foreach ($categories as $category => $products) {
                $count = 0;
                foreach ($products as $product => $quantity) {
                    $productsPerMonth[$month][$category][$count]['name'] = $this->speller->verifyWord($product);
                    $productsPerMonth[$month][$category][$count]['quantity'] = $quantity;
                    $count++;
                }
            }
        }

        return $productsPerMonth;
    }



    private function orderByMonth($cleanedListOfShopping)
    {
        $productPerMonthOrdered = [];
        foreach ($this->months as $month) {
            foreach ($cleanedListOfShopping as $productMonth => $categories) {
                if ($productMonth == $month) {
                    $productPerMonthOrdered[$productMonth] = $categories;
                }
            }
        }

        return $productPerMonthOrdered;
    }


    private function orderByCategory($productPerMonthOrdered)
    {
        $orderByCategory = [];
        foreach ($productPerMonthOrdered as $month => $categories) {
            ksort($categories);
            $orderByCategory[$month] = $categories;
        }

        return $orderByCategory;
    }


    private function formatProductsByNameAndQuantity($productPerMonthOrdered, $orderBy = SORT_ASC)
    {
        $formatProductsByNameAndQuantity = [];
        foreach ($productPerMonthOrdered as $month => $categories) {
            foreach ($categories as $category => $products) {

                $sort = [];
                $orderProducts = $products;

                foreach ($products as $product => $value) {
                    $sort['name'][$product] = $value['name'];
                    $sort['quantity'][$product] = $value['quantity'];
                }

                array_multisort($sort['name'], $orderBy, $sort['quantity'], $orderBy, $orderProducts);

                $formatProductsByNameAndQuantity[$month][$category] = $orderProducts;
            }
        }

        return $formatProductsByNameAndQuantity;
    }
}
