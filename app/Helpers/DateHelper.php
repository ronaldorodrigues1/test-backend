<?php

namespace App\Helpers;

class DateHelper
{
    public  function getMonthIdByName($monthName)
    {
        return array_search($monthName, $this->getMonths());
    }

    public function getMonthNameById($id)
    {
        $months = $this->getMonths();
        return $months[$id];
    }

    public function getMonths()
    {
        return [
            'janeiro',  'fevereiro',
            'marco',    'abril',
            'maio',     'junho',
            'julho',    'agosto',
            'setembro', 'outubro',
            'novembro', 'dezembro'
        ];
    }
}
