<?php

namespace App\Helpers;

class SpellerHelper
{
    protected $corrections = [
        "Papel Higiênico", "Brócolis", "Chocolate ao leite", "Sabão em pó"
    ];

    public function verifyWord($wordToVerify)
    {
        $shortest = -1;

        foreach ($this->corrections as $word) {
            $levResult = levenshtein($wordToVerify, $word);

            if ($levResult <= $shortest || $shortest < 0) {
                $wordToVerify  = $word;
                $shortest = $levResult;
            }
        }

        return $wordToVerify;
    }
}
