<table class="table table-dark"">
    <thead>
        <tr>
            <th >Mês</th>
            <th>Categoria</th>
            <th>Produto</th>
            <th>Quantidade</th>
        </tr>
    </thead>

 
    <tbody>

    @foreach ($products as $product)
        <tr>
            <td>{{ $product['month'] }}</td>
            <td>{{ $product['category']['data']['name'] }}</td>
            <td>{{ $product['name'] }}</td>
            <td>{{ $product['quantity'] }}</td>
        </tr>
        @endforeach
    </tbody>
    
</table> 