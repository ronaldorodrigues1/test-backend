@extends('layouts.layout')
@section('title', 'Produtos Akna')

@section('content')
   <div class="container">
      @include('inc.products.table', $products)
   </div>
@endsection